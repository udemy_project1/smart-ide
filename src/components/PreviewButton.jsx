import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { togglePreview } from "../features/preview";

import showView from "../assets/view.svg";
import hideView from "../assets/hide.svg";

export default function PreviewButton() {
	const previewData = useSelector((state) => state.preview);
	const dispatch = useDispatch();

	return (
		<button
			className="py-2 px-4 rounded bg-blue-700 flex mx-auto items-center justify-center text-slate-50"
			onClick={() => dispatch(togglePreview())}>
			<img
				className="w-5 h-5 mr-1"
				src={previewData.preview ? hideView : showView}
				alt="button preview"
			/>
			<span>{previewData.preview ? "Hide" : "Show"} Preview</span>
		</button>
	);
}
